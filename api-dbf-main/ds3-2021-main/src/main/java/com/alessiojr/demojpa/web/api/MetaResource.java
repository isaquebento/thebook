package com.alessiojr.demojpa.web.api;

import com.alessiojr.demojpa.domain.Meta;
import com.alessiojr.demojpa.service.MetaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/metas")
public class MetaResource {
    private final Logger log = LoggerFactory.getLogger(MetaResource.class);

    private final MetaService metaService;

    public MetaResource(MetaService metaService) {
        this.metaService = metaService;
    }

    /**
     * {@code GET  /metas/:id} : get the "id" meta.
     *
     * @param id o id do meta que será buscado.
     * @return o {@link ResponseEntity} com status {@code 200 (OK)} e no body o meta, ou com status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Meta> getMeta(@PathVariable Long id) {
        log.debug("REST request to get Meta : {}", id);
        Optional<Meta> meta = metaService.findOne(id);
        if(meta.isPresent()) {
            return ResponseEntity.ok().body(meta.get());
        }else{
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/")
    public ResponseEntity<List<Meta>> getMetas(){
        List<Meta> lista = metaService.findAllList();
        if(lista.size() > 0) {
            return ResponseEntity.ok().body(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * {@code PUT  /metas} : Atualiza um meta existenteUpdate.
     *
     * @param meta o meta a ser atulizado.
     * @return o {@link ResponseEntity} com status {@code 200 (OK)} e no corpo o meta atualizado,
     * ou com status {@code 400 (Bad Request)} se o meta não é válido,
     * ou com status {@code 500 (Internal Server Error)} se o meta não pode ser atualizado.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/")
    public ResponseEntity<Meta> updateMeta(@RequestBody Meta meta) throws URISyntaxException {
        log.debug("REST request to update Meta : {}", meta);
        if (meta.getId() == null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Invalid Meta id null");
        }
        Meta result = metaService.save(meta);
        return ResponseEntity.ok()
                .body(result);
    }

    /**
     * {@code POST  /} : Create a new meta.
     *
     * @param meta the meta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meta, or with status {@code 400 (Bad Request)} if the meta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/")
    public ResponseEntity<Meta> createMeta(@RequestBody Meta meta) throws URISyntaxException {
        log.debug("REST request to save Meta : {}", meta);
        if (meta.getId() != null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Um novo meta não pode terum ID");
        }
        Meta result = metaService.save(meta);
        return ResponseEntity.created(new URI("/api/metas/" + result.getId()))
                .body(result);
    }

    @PostMapping(value = "/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Meta> upload(@RequestPart("data") MultipartFile csv) throws IOException {
        List<Meta> savedNotes = new ArrayList<>();
        List<Meta> notes = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(csv).getInputStream(), StandardCharsets.UTF_8)).lines()
                .map(Meta::parseNote).collect(Collectors.toList());
        metaService.saveAll(notes).forEach(savedNotes::add);
        return savedNotes;
    }

    /**
     * {@code DELETE  /:id} : delete pelo "id" meta.
     *
     * @param id o id do metas que será delete.
     * @return o {@link ResponseEntity} com status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMeta(@PathVariable Long id) {
        log.debug("REST request to delete Meta : {}", id);

        metaService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
