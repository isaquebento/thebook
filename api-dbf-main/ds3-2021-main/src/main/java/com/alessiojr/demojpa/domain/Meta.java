package com.alessiojr.demojpa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.util.Date;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "table_meta")
public class Meta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int numLivros;
    private Date dataInicio;
    private Date dataFinal;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    private Boolean isActive;

    public static Meta parseNote(String line) {
        String[] text = line.split(",");
        Meta note = new Meta();
        note.setId(Long.parseLong(text[0]));
        return note;
    }
}
